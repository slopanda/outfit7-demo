package si.medvedanze.outfit7.demo;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import si.medvedanze.outfit7.demo.model.ServicesStatus;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CheckServicesControllerTest {

    @LocalServerPort
    private int port;
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testCheckService() throws Exception {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<ServicesStatus> response = restTemplate.exchange(
                createURLWithPort("/checkServices?cc=US&timezone=America/Chihuahua&userId=42"), HttpMethod.GET, entity, ServicesStatus.class);
        assertFalse(response.getBody().getMultiplayer().equals("enabled"));
    }

    @Test
    public void testMultiplayerEnabled() throws Exception {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<ServicesStatus> response = restTemplate.exchange(
                createURLWithPort("/checkServices?cc=US&timezone=America/Chihuahua&userId=23"), HttpMethod.GET, entity, ServicesStatus.class);
        assertTrue(response.getBody().getMultiplayer().equals("enabled"));
    }

    @Test
    public void testMultiplayerDisabled() throws Exception {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<ServicesStatus> response = restTemplate.exchange(
                createURLWithPort("/checkServices?cc=SI&timezone=America/Chihuahua&userId=23"), HttpMethod.GET, entity, ServicesStatus.class);
        assertFalse(response.getBody().getMultiplayer().equals("enabled"));
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}
