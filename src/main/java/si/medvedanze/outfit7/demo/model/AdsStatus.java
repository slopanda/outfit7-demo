package si.medvedanze.outfit7.demo.model;

/**
 * Class representation of current ads status.
 */
public class AdsStatus {

    private Integer adsHttpResponseCode;
    private Boolean adsEnabled;

    public AdsStatus(Integer adsHttpResponseCode, Boolean adsEnabled) {
        this.adsHttpResponseCode = adsHttpResponseCode;
        this.adsEnabled = adsEnabled;
    }

    public Integer getAdsHttpResponseCode() {
        return adsHttpResponseCode;
    }

    public Boolean getAdsEnabled() {
        return adsEnabled;
    }
}
