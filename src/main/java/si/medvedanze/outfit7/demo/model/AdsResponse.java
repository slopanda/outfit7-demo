package si.medvedanze.outfit7.demo.model;

/**
 * Ads service response class representation.
 */
public class AdsResponse {
    private String ads;

    public String getAds() {
        return ads;
    }

    public void setAds(String ads) {
        this.ads = ads;
    }
}
