package si.medvedanze.outfit7.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representation of service status.
 */
public class ServicesStatus {

    private String multiplayer;

    @JsonProperty("user-support")
    private String userSupport;

    private String ads;

    public String getMultiplayer()
    {
        return multiplayer;
    }

    public void setMultiplayer(String multiplayer)
    {
        this.multiplayer = multiplayer;
    }

    public String getUserSupport()
    {
        return userSupport;
    }

    public void setUserSupport(String userSupport)
    {
        this.userSupport = userSupport;
    }

    public String getAds()
    {
        return ads;
    }

    public void setAds(String ads)
    {
        this.ads = ads;
    }
}
