package si.medvedanze.outfit7.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import si.medvedanze.outfit7.demo.model.AdsResponse;
import si.medvedanze.outfit7.demo.model.AdsStatus;


@Service
public class AdsService {

    private static final String ADS_URI = "https://us-central1-o7tools.cloudfunctions.net/fun7-ad-partner/";

    Logger logger = LoggerFactory.getLogger(AdsService.class);

    /**
     * Function calls ads API to check if ads are enabled for given country code.
     * @param countryCode - country code to be checked in ads API
     * @param uname - API authentication username
     * @param pass - API authentication password
     * @return AdsStatus instance containing response code and ads status
     */
    public AdsStatus areAdsEnabled(String countryCode, String uname, String pass)
    {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        ResponseEntity<AdsResponse> response;

        try {
            response = restTemplateBuilder.basicAuthentication(uname,pass)
                                          .build()
                                          .getForEntity(prepareUri(countryCode),AdsResponse.class);

            logger.info("Received response from Ads API: "+response.getBody().toString());
        }
        catch(HttpServerErrorException exception)
        {
            logger.error("Ads API return internal server error (500) ! Considering ads as unavailable !");
            return new AdsStatus(500, false);
        }

        return new AdsStatus(response.getStatusCode().value(), response.getBody().getAds().contains("sure"));
    }

    /**
     * Helper function to prepare URI with requested parameters.
     * @param countryCode - value to be added as an URI parameter
     * @return formatted URI
     */
    private String prepareUri(String countryCode)
    {
        return ADS_URI+"?countryCode="+countryCode;
    }

}
