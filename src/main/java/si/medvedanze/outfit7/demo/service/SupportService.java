package si.medvedanze.outfit7.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.TimeZone;

@Service
public class SupportService {
    public static final Integer SUPPORT_HOUR_START = 9;
    public static final Integer SUPPORT_HOUR_END = 15;
    public static final String  SUPPORT_TIMEZONE = "Europe/Ljubljana";

    Logger logger = LoggerFactory.getLogger(SupportService.class);

    /**
     * Function checks if support is reachable in given moment.
     * @return true if current time in Ljubljana is between SUPPORT_HOUR_START and SUPPORT_HOUR_END and there is workday,
     *              public holidays are currently not supported
     *         false otherwise
     */
    public boolean isSupportReachable()
    {
        Calendar calendar = Calendar.getInstance();
        TimeZone supportTZ = TimeZone.getTimeZone(SUPPORT_TIMEZONE);
        calendar.setTimeZone(supportTZ);

        Integer supportCurrentHour = calendar.get(Calendar.HOUR_OF_DAY);
        Integer supportCurrentDay = calendar.get(Calendar.DAY_OF_WEEK);

        logger.info("Currently is day #"+supportCurrentDay+" and "+supportCurrentHour+" hours in Ljubljana !");

        return supportCurrentHour >= SUPPORT_HOUR_START && supportCurrentHour < SUPPORT_HOUR_END &&
                supportCurrentDay >= Calendar.MONDAY && supportCurrentDay < Calendar.SATURDAY;
    }
}
