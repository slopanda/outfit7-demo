package si.medvedanze.outfit7.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

@Service
public class MultiplayerService {

    private static final int MIN_API_CALLS_FOR_MULTIPLAYER = 5;

    @Autowired
    JdbcTemplate jdbcTemplate;
    private Connection connection;

    Logger logger = LoggerFactory.getLogger(MultiplayerService.class);

    /**
     * Function checks if user based on provided userId should have multiplayer enabled or not.
     * @param userId - user identification
     * @param countryCode - country code of User
     * @return true if more than MIN_API_CALLS_FOR_MULTIPLAYER and countryCode is US, otherwise false
     */
    public boolean isMultiplayerEnabled(String userId, String countryCode)
    {
        int numCalls = 0;

        CallableStatement callableStatement = null;

        try
        {
            logger.info("Trying to fetch API access data for user "+userId+" !");

            // prepare call
            connection = jdbcTemplate.getDataSource().getConnection();
            callableStatement = connection.prepareCall("{? = call get_user_api_access(?)}");
            callableStatement.setString(2, userId);
            callableStatement.registerOutParameter(1, Types.INTEGER);
            callableStatement.execute();

            // fetch result
            numCalls = callableStatement.getInt(1);
            logger.info("Fetch successful! User "+userId+" accessed API for "+numCalls+" times !");
        }
        catch (Exception throwables)
        {
            logger.error("Fetch failed with exception: "+throwables.getMessage());
            throwables.printStackTrace();
        }
        finally
        {
            try
            {
                // clean up
                callableStatement.close();
                connection.close();
            } catch (SQLException throwables)
            {
                logger.error("Connection cleanup failed: "+throwables.getMessage());
            }
        }

        return (numCalls > MIN_API_CALLS_FOR_MULTIPLAYER) && "US".equalsIgnoreCase(countryCode);
    }
}
