package si.medvedanze.outfit7.demo.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import si.medvedanze.outfit7.demo.model.AdsStatus;
import si.medvedanze.outfit7.demo.model.ServicesStatus;
import si.medvedanze.outfit7.demo.service.AdsService;
import si.medvedanze.outfit7.demo.service.MultiplayerService;
import si.medvedanze.outfit7.demo.service.SupportService;

@Controller
public class CheckServicesController {

    @Autowired
    private AdsService adsService;

    @Autowired
    private MultiplayerService multiplayerService;

    @Autowired
    private SupportService supportService;

    /**
     * Implementation of API endpoint that checks services status for given user, timezone and country code.
     * @param timezone - user timezone (not considered inside API, because only server timezone and support timezone
     *                   are important for decision)
     * @param userId - user id
     * @param cc - country code of user
     * @return report of service status for given user (refer to ServiceStatus implementation)
     */
    @RequestMapping(value = "checkServices",
            method = RequestMethod.GET,
            produces={MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Check services status for given user",
            notes = "Returns status of ads, multiplayer and support services",
            response = ServicesStatus.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
    public @ResponseBody ResponseEntity checkServicesStatus(@ApiParam(value = "User timezone", required = true)
                                                            @RequestParam String timezone,
                                                            @ApiParam(value = "User identification", required = true)
                                                            @RequestParam String userId,
                                                            @ApiParam(value = "User country code", required = true)
                                                            @RequestParam String cc)
    {
        AdsStatus adsStatus = adsService.areAdsEnabled(cc,"fun7user","fun7pass");
        boolean supportReachable = supportService.isSupportReachable();
        boolean multiplayerEnabled = multiplayerService.isMultiplayerEnabled(userId, cc);

        ServicesStatus servicesStatus = new ServicesStatus();
        servicesStatus.setAds(adsStatus.getAdsEnabled().booleanValue() ? "enabled" : "disabled");
        servicesStatus.setMultiplayer(multiplayerEnabled ? "enabled" : "disabled");
        servicesStatus.setUserSupport(supportReachable ? "enabled" : "disabled");

        return new ResponseEntity<>(servicesStatus, HttpStatus.OK);
    }
}
