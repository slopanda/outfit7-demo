CREATE TABLE USERS (
  user_id VARCHAR(255) NOT NULL,
  api_access_cnt INT NOT NULL,
  PRIMARY KEY (user_id)
);

CREATE OR REPLACE FUNCTION get_user_api_access(p_user_id   IN USERS.user_id%TYPE)
    RETURNS INT
    language plpgsql
AS $$
DECLARE
    l_exist INT;
    l_cnt INT;
BEGIN
    SELECT count(*)
      INTO l_exist
      FROM USERS
     WHERE user_id = p_user_id;
        
    IF l_exist = 0 THEN
        INSERT INTO USERS(user_id, api_access_cnt)
             VALUES (p_user_id, 1);
        RETURN 1;
    ELSE
        UPDATE USERS 
           SET api_access_cnt = api_access_cnt + 1
         WHERE user_id = p_user_id;
    END IF;

    SELECT api_access_cnt
      INTO l_cnt
      FROM USERS
     WHERE user_id = p_user_id;

    RETURN l_cnt;
END;
$$