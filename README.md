# Outfit 7 Software Engineer Expertise Test: Services

This repository contains solution of Software Engineer Expertise Test: Services
task completed by Anže Medved (https://www.linkedin.com/in/anže-medved-a1685aab/).

### Building the application
```
gradle bootJar
```

### Running the application
```
java -jar build/libs/demo-1.0-RELEASE.jar
```

### Accessibility

* [Google cloud sample call](https://sage-now-284610.ew.r.appspot.com/checkServices?cc=US&timezone=America/Chihuahua&userId=351223)
* [Google cloud API Swagger documentation](https://sage-now-284610.ew.r.appspot.com/swagger-ui.html)

### Assumptions
* Support is available every day between Monday and Friday, 9:00 - 15:00, Ljubljana local time (local public holidays are not supported)
* User timezone is not considered, because service only translates server current time to Ljubljana local time
* More than 5 calls to API should be made with given userId to enable multiplayer service. All API calls are considered, including those outside US country code. When checking if user has multiplayer enabled, US country code is required.
* I made an assumption that TZ database name format is used ([Wikipedia page](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones))

### Disclaimer
* Data is stored on free instance of ElephantSql (https://www.elephantsql.com) so concurrent connections are limited.

